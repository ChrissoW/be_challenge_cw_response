from django.db import models

# Create your models here.

class Employees(models.Model):
	first_name = models.CharField(max_length=200)
	last_name = models.CharField(max_length=200)

	def __str__ (self):
		return self.first_name + ', ' + self.last_name

class Shifts(models.Model):
	#The following fields are CharField for the sake of this exercise.
	# For a full blown application they would be timestamp.
	date = models.CharField(max_length=50)
	start = models.CharField(max_length=50)
	end = models.CharField(max_length=50)
	break_time = models.CharField(max_length=50)
	shift_number = models.IntegerField()


