from django.urls import path

from . import views

urlpatterns = [
	path('', views.index, name='index'),

	#employees/id/
	#path(r'^(?P<employee_id>[0-9]+)/$', views.employees, name='employees'),

	path('employees/', views.employees, name='employees'),

	path('employees/<int:employee_id>', views.employees_detail, name='employees_detail'),

	path('shifts/', views.shifts, name='shifts'),

	path('set_roster/', views.set_roster, name='set_roster'),
]