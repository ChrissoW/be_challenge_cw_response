from django.shortcuts import render
from django.http import HttpResponse
from .models import Employees, Shifts

import random



def index(request):
	return HttpResponse("Welcome to the Roster app.")


def employees(request):

	all_employees = Employees.objects.all()
	context = {'all_employees': all_employees}
	return render(request, 'app/employees.html', context)


def employees_detail(request, employee_id):

	employee = Employees.objects.get(id=employee_id)
	context = {'employee': employee}
	return render(request, 'app/employee_details.html', context)


def shifts(request):

	shifts = Shifts.objects.all()
	context = {'shifts': shifts}
	return render(request, 'app/shifts.html', context)


def set_roster(request):

	shifts_raw = Shifts.objects.all()

	shifts_organised = []

	for shift in shifts_raw:
		new_shift = []
		new_shift.append(shift.id)
		new_shift.append(shift.date)
		new_shift.append(shift.shift_number)
		new_shift.append(shift.start)
		new_shift.append(shift.end)
		shifts_organised.append(new_shift)

	employees_list = []

	employees = Employees.objects.all()

	for employee in employees:
		#employees_list.append(employee.last_name)
		employees_list.append([employee.first_name, employee.last_name])

	#This will be a random pick of an employee. It represents, say, the manager choosing an employee for a given shift.
	# This could be done through a web page.
	def pick_employee_for_shift(employees_list):
		#Max number has to be one less than the length.
		maximum = len(employees_list) - 1

		if maximum != 0:
			for i in range(maximum):
				picked_employee = employees_list[random.randint(0, maximum)]
				return picked_employee
		#If i=0 then the random function doesn't work, and then no one gets picked.
		else:
			picked_employee = employees_list[0]
			return(picked_employee)

	#candidates = set(employees_list)
	candidates = employees_list[:]

	#This is to get the first shift_date
	shift_date = shifts_organised[0][1]
	shift_number = 1
	#Faster for checking if something is 'in' if it's a set.
	this_shift = set()
	last_shift = set()
	shifts_fixed = []

	for shift in shifts_organised:

		#THought of resetting 'candidates' as per below. But that wouldn't be fair.
		# It means some of the people could get more than one shift while others get none.

		# if shift[1] != shift_date:
		# 	#Have to do this with '[:]' or else this doesn't work.
		# 	candidates = employees_list[:]
		# 	shift_date = shift[1]

		#Checking to see if candidates is empty. If so, fill it up again.
		# This way the list resets only after everyone has had a shift.
		if not candidates:
			candidates = employees_list[:]

		#Pick an employee from those available
		while True:
			picked_employee = pick_employee_for_shift(candidates)

			#Might as well just check the last name.
			if picked_employee[1] not in last_shift:
				#We are adding the name of the employee to the shift.
				shift.append(picked_employee)
				#And now we add it to whole shift list
				shifts_fixed.append(shift)

				#Take them out of the candidates list. Because they have been picked.
				candidates.remove(picked_employee)
				#And then and them to the 'this shift' to make sure no one had consequitive shifts.
				this_shift.add(picked_employee[1])
				break

		#If the shift changes, then 'this shift' has to become 'last shift'.
		if shift[2] != shift_number:
			last_shift = this_shift
			this_shift = set()
			shift_number = shift[2]


	shifts_display = []
	for shift in shifts_fixed:

		#Am formatting the 'shift string' here because it seems indexes won't work on the template side. I think.
		shift_display = str(shift[0]) + ', ' + str(shift[1]) + ', ' + str(shift[3]) + ' - ' + str(shift[4]) + ', ' + shift[5][0] + ' ' + shift[5][1]
		shifts_display.append(shift_display)


	context = {'shifts_display': shifts_display}
	return render(request, 'app/set_roster.html', context)












